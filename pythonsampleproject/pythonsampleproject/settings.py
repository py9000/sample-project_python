from pythonsampleproject.derived_settings import APPDIR, SETUPFILEDIR, TESTDIR, MEMTEMPDIR

VERSION = "0.0.3"
PRINT_VERBOSITY = "high"
EXCLUDED_DIRS = [".DS_Store"]
PROJECT_NAME = "pythonsampleproject"
TEMPDIR = "/tmp"
TEXTTABLE_STYLE = ["-", "|", "+", "-"]
DIRS = [f"{TEMPDIR}/pythonsampleprojectworkingdirs"]
MINIMUM_PYTHON_VERSION = (3, 6, 9)
COVERAGERC_PATH = f"{APPDIR}/.coveragerc"
